console.log("Hello NPM ikaa")

// ----------------------------------------------------

// Ini akan mengimport module bernama 'os'
const os = require('os')
console.log("Free memory: ", os.freemem())

// ----------------------------------------------------

const luasSegitiga = require('./segitiga.js')
// contoh import module local
console.log("Luas Segitiga: ", luasSegitiga(3,4), "cm")

// ----------------------------------------------------

// Ini akan mengimport module bernama 'fs'
const fs = require('fs')
const isi = fs.readFileSync('./text.txt', 'utf-8')
console.log(isi)
fs.writeFileSync('./text2.txt', 'I love Binar <3')
const newText = fs.readFileSync('./text2.txt', 'utf-8')
console.log(newText)

// ----------------------------------------------------

const createPerson =  function(person) {
    fs.writeFileSync('./person.json', JSON.stringify(person))
    return person;
}
const Rizka = createPerson({
    name: 'Rizka',
    age: 20,
    address: 'Solo',
    gender: 'female'
})
// const fs = require('fs')
const rizka = require('./person.json')
console.log(rizka)

console.log("")